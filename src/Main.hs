{-# LANGUAGE BangPatterns #-}
module Main where

data MemTree a = MemTree (MemTree a) a (MemTree a)

instance Functor MemTree where
  fmap f (MemTree l m r) = MemTree (fmap f l) (f m) (fmap f r)

nats :: MemTree Integer
nats = go 0 1
    where
        go !n !s = MemTree (go l s') n (go r s')
            where
                l = n + s
                r = l + s
                s' = s * 2

-- | Nat to nth member of tree.
index :: MemTree a -> Integer -> a
index (MemTree l m r) n
  | 0 <- n = m
  | (q, 0) <- (n - 1) `divMod` 2 = index l q
  | (q, 1) <- (n - 1) `divMod` 2 = index r q
  | otherwise = undefined

-- | Take a nat and gives a int.
natInt :: Integer -> Integer
natInt n
  | odd n = - ((n - 1) `div` 2) - 1
  | otherwise = n `div` 2

-- | Take an int and gives a nat.
intNat :: Integer -> Integer
intNat n
  | n >= 0 = 2*n
  | otherwise = (-2*n) - 1

-- | Takes a pair of nats and makes a nat
pairNat :: (Integer, Integer) -> Integer
pairNat (l, r) = ((l + r)*(l + r + 1) `div` 2) + r

-- | Takes a pair of nats and makes a nat
natPair :: Integer -> (Integer, Integer)
natPair n = (w - y, y)
  where t = (w^2 + w) `div` 2
        w = floor (((sqrt . fromIntegral) (8*n + 1) - 1.0) / 2.0)
        y = n - t

-- | Takes a nat and produces a triple of two ints and a nat
natTriple :: Integer -> (Integer, Integer, Integer)
natTriple n = (natInt x, natInt y, z)
  where (next, z) = natPair n
        (x, y) = natPair next

-- | Takes a triple of two ints and a nat and produces a nat
tripleNat :: (Integer, Integer, Integer) -> Integer
tripleNat (x, y, z) = pairNat (next, z)
  where next = pairNat (intNat x, intNat y)


ca :: ((Integer -> Integer -> a) -> Integer -> Integer -> a) -> (Integer -> Integer -> a) -> (Integer -> Integer -> Integer -> a)
ca rules initial = lookup
  where golTree = fmap (go . natTriple) nats
        recurse t = index golTree $ tripleNat t
        go (x, y, 0) = initial x y
        go (x, y, z) = rules (\x' y' -> recurse (x', y', z - 1)) x y
        lookup x y z = recurse (x, y, z)

golRules :: (Integer -> Integer -> Bool) -> Integer -> Integer -> Bool
golRules recurse x y = survive || born
  where recursePair (x', y') = recurse x' y'
        alive_last = recurse x y
        neighbours = (length . filter recursePair) [(x + dx, y + dy) | dx <- [-1.. 1], dy <- [-1.. 1], dx /= 0 || dy /= 0]
        survive = alive_last && (neighbours == 2 || neighbours == 3)
        born = not alive_last && (neighbours == 3)

gol :: (Integer -> Integer -> Bool) -> (Integer -> Integer -> Integer -> Bool)
gol = ca golRules

displayGol :: (Integer, Integer) -> (Integer, Integer) -> Integer -> String
displayGol (startx, starty) (endx, endy) t = unlines [[displayCell $ golInstance x y t | x <- [startx.. endx]] | y <- [starty.. endy]]
  where init (-10) (-8) = True
        init (-9) (-8) = True
        init (-8) (-8) = True
        init (-8) (-9) = True
        init (-9) (-10) = True
        init _ _ = False
        golInstance = gol init
        displayCell True = '#'
        displayCell False = '.'

main :: IO ()
main = putStr $ displayGol (-10, -10) (10, 10) 40
